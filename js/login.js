import { iniciarSesion } from "./config.js";

const login = document.querySelector("#login");
const limpiar = document.querySelector("#limpiar");

login.addEventListener("click", async () => {
	event.preventDefault();
	let user = document.querySelector("#email").value.trim();
	let pass = document.querySelector("#password").value.trim();

	if (!user || !pass) return alert("Rellena los campos vacios");

	try {
		await iniciarSesion(user, pass);
	} catch (error) {
		alert("Datos incorrectos");
	}
});

limpiar.addEventListener("click", () => {
	event.preventDefault();
	document.querySelector("#email").value = "";
	document.querySelector("#password").value = "";
});
