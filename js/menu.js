"use strict";
// Import the functions you need from the SDKs you need
import { signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { auth } from "./config.js";

document.querySelector("#cerrarSesion").addEventListener("click", async e => {
	e.preventDefault();

	await signOut(auth);
});
