"use strict";
// Import the functions you need from the SDKs you need
import { onValue, ref, set, child, get, update } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
import { signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { db, storage, auth } from "./config.js";

// Declarar elementos del DOM
const resultados = document.querySelector("#resultados");
const resultadosCarro = document.querySelector("#resultadosCarro");
const imagen = document.querySelector("#imagen");
var cesta = 0;
var total = 0;
// Variables input
const obtenerCampos = () => {
	event.preventDefault();

	return {
		codigo: document.querySelector("#codigo").value.trim(),
		nombre: document.querySelector("#nombre").value.trim(),
		precio: document.querySelector("#precio").value.trim(),
		stock: document.querySelector("#stock").value.trim(),
		cantidad: document.querySelector("#cantidad").value.trim(),
		cantidad: document.querySelector("#total").value.trim(),
	};
};

const limpiarCampos = () => {
	event.preventDefault();
	document.querySelector("#codigo").value = "";
	document.querySelector("#nombre").value = "";
	document.querySelector("#precio").value = "";
	document.querySelector("#cantidad").value = "";
	document.querySelector("#total").value = "";
	document.querySelector("#vistaPrevia").classList.add("d-none");
	document.querySelector("#nombreInvisible").classList.add("d-none");
	document.querySelector("#precioInvisible").classList.add("d-none");
	document.querySelector("#stockInvisible").classList.add("d-none");
	total = 0;
};

const limpiarCamposAñadiendo = () => {
	event.preventDefault();
	document.querySelector("#codigo").value = "";
	document.querySelector("#nombre").value = "";
	document.querySelector("#precio").value = "";
	document.querySelector("#cantidad").value = "";
	document.querySelector("#vistaPrevia").classList.add("d-none");
	document.querySelector("#nombreInvisible").classList.add("d-none");
	document.querySelector("#precioInvisible").classList.add("d-none");
	document.querySelector("#stockInvisible").classList.add("d-none");

};

const llenarCampos = ({ codigo, nombre, precio, url, stock }) => {
	document.querySelector("#codigo").value = codigo;
	document.querySelector("#nombre").value = nombre;
	document.querySelector("#precio").value = precio;
	document.querySelector("#stock").value = stock;
	document.querySelector("#vistaPrevia").src = url;
	document.querySelector("#vistaPrevia").classList.remove("d-none");
	document.querySelector("#nombreInvisible").classList.remove("d-none");
	document.querySelector("#precioInvisible").classList.remove("d-none");
	document.querySelector("#stockInvisible").classList.remove("d-none");
};

const cambiarImagen = () => {
	document.querySelector("#vistaPrevia").src = URL.createObjectURL(imagen.files[0]);
	document.querySelector("#vistaPrevia").classList.remove("d-none");
};

async function insertarProducto() {
	try {
		event.preventDefault();

		let { codigo, nombre, precio, stock } = obtenerCampos();
		const dbref = ref(db);
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (!codigo || !nombre) {
			return alert("¡Busque un producto!");
		}
		if (stock <= 0) {
			return alert("¡No hay stock disponible!");
		} else {
			const snapshot = await get(child(dbref, "productos/" + codigo));
			let url = await getDownloadURL(storageRef);

			if (cantidad.value == 0) {
				return alert("¡Escriba una cantidad!");
			} else {
				if (cantidad.value > stock) {
					return alert("¡No hay suficiente stock!");
				} else {

					parseInt(cantidad.value);
					parseInt(snapshot.val().precio);
					parseInt(total);
					total = total + (cantidad.value * snapshot.val().precio);
					document.querySelector("#total").value = total;

					alert("¡Se añadió al carrito!");
					limpiarCamposAñadiendo();
				}
		}
	}
	} catch (error) {
		if (error.code === "PERMISSION_DENIED" || error.code === "storage/unauthorized") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}

async function mostrarProductosCarro() {
	event.preventDefault();
	try {
		const dbref = ref(db, "productos");

		await onValue(dbref, snapshot => {
			resultadosCarro.innerHTML = `<thead><tr>
					<th scope="col" width="5%">Código</th>
					<th scope="col" width="5%">Nombre</th>
					<th scope="col" width="5%">Stock</th>
					<th scope="col" width="5%">Precio</th>
					<th scope="col" width="1%">Imagen</th>
				</tr></thead><tbody></tbody>`;

			snapshot.forEach(childSnapshot => {
				const childKey = childSnapshot.key;
				const childData = childSnapshot.val();

				resultadosCarro.lastElementChild.innerHTML += `<tr>
					<th scope="row">${childKey}</th>
					<td>${childData.nombre}</td>
					<td>${childData.stock}</td>
					<td>$${childData.precio}</td>
					<td class="p-0"><img class="w-100" src="${childData.url}" alt="Imagen de ${childData.nombre}"/></td>
				</tr>`;
			});
		});

		resultadosCarro.classList.remove("d-none");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}
async function mostrarProductos() {
	event.preventDefault();

	try {
		const dbref = ref(db, "productos");

		await onValue(dbref, snapshot => {
			resultados.innerHTML = `<thead><tr>
			<th scope="col" width="5%">Código</th>
			<th scope="col" width="5%">Nombre</th>
			<th scope="col" width="5%">Stock</th>
			<th scope="col" width="5%">Precio</th>
			<th scope="col" width="1%">Imagen</th>
				</tr></thead><tbody></tbody>`;

				snapshot.forEach(childSnapshot => {
					const childKey = childSnapshot.key;
					const childData = childSnapshot.val();
					if (childData.status == 1) {
					resultados.lastElementChild.innerHTML += `<tr>
					<th scope="row">${childKey}</th>
					<td>${childData.nombre}</td>
					<td>${childData.stock}</td>
					<td>$${childData.precio}</td>
					<td class="p-0"><img class="w-100" src="${childData.url}" alt="Imagen de ${childData.nombre}"/></td>
				</tr>`;
					}
				});
		});
		resultados.classList.remove("d-none");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}
async function buscarProducto() {
	try {
		event.preventDefault();

		let { codigo } = obtenerCampos();
		if (codigo === "") return alert("Escriba un código");

		const dbref = ref(db);
		const snapshot = await get(child(dbref, "productos/" + codigo));
		if (snapshot.val().status == 1){

		
		if (snapshot.exists()) {
			let nombre = snapshot.val().nombre;
			let precio = snapshot.val().precio;
			let stock = snapshot.val().stock;
			let url = snapshot.val().url;

			llenarCampos({
				codigo,
				nombre,
				precio,
				stock,
				url,
			});

		} else {
			alert("No se encontró ese producto");
		}
	}else{
		alert("¡Producto no disponible!");
	}
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}
async function actualizarProducto() {
	try {
		event.preventDefault();

		let { codigo, nombre, descripcion, precio, stock } = obtenerCampos();
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (isNaN(parseFloat(precio)) || parseFloat(precio) <= 0) {
			return alert("Escriba un precio válido");
		}

		if (!codigo || !nombre || !descripcion) {
			return alert("Rellene los campos faltantes");
		}

		if (!imagen.value) {
			await update(ref(db, "productos/" + codigo), {
				nombre,
				descripcion,
				precio: precio,
				stock: stock,
			});
			return alert("Se realizó una actualización");
		}

		await uploadBytes(storageRef, imagen.files[0]);
		let url = await getDownloadURL(storageRef);

		await update(ref(db, "productos/" + codigo), {
			nombre,
			descripcion,
			precio: precio,
			stock,
			url,
		});
		return alert("Se realizó una actualización");
	} catch (error) {
		if (error.code === "PERMISSION_DENIED" || error.code === "storage/unauthorized") {
			alert("No estás autentificado");
		} else {
			console.error(error);
		}
	}
}
async function quitarProducto() {
	try {
		event.preventDefault();

		let { codigo, nombre, precio } = obtenerCampos();
		const dbref = ref(db);
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (!codigo || !nombre) {
			return alert("¡Busque un producto!");
		}

		const snapshot = await get(child(dbref, "productos/" + codigo));

		let url = await getDownloadURL(storageRef);

		if (cantidad.value == 0) {
			return alert("¡Escriba una cantidad!");
		} else {

			parseInt(cantidad.value);
			parseInt(snapshot.val().precio);
			parseInt(total);

			cesta++;
			total = total - (cantidad.value * snapshot.val().precio);

			document.querySelector("#total").value = total;

			alert("Se ha eliminado del carrito");
			limpiarCamposAñadiendo();
		}
	} catch (error) {
		if (error.code === "PERMISSION_DENIED" || error.code === "storage/unauthorized") {
			alert("No cuentas con permisos");
		} else {
			console.error(error);
		}
	}
}
async function desactivarProducto() {
	try {
		event.preventDefault();

		let { codigo } = obtenerCampos();
		if (codigo === "") return alert("Introduzca un código");
		const dbref = ref(db);

		const snapshot = await get(child(dbref, "productos/" + codigo));
		if (!snapshot.exists()) {
			return alert("No existe un producto con ese código");
		}

		if (snapshot.val().status === "0") {
			await update(ref(db, "productos/" + codigo), { status: "1" });
			alert("El registro fue activado");
		} else {
			await update(ref(db, "productos/" + codigo), { status: "0" });
			alert("El registro fue desactivado");
		}

		await mostrarProductos();
	} catch (error) {
		if (error.code === "PERMISSION_DENIED") {
			alert("No estás autentificado");
		} else {
			console.error(error);
		}
	}
}
async function terminarVenta() {
	window.alert("Venta realizada!");
	total = 0;
}

window.onload = () => {
	mostrarProductos();
	// mostrarProductosCarro();
}

document.querySelector("#btnAgregar").addEventListener("click", insertarProducto);
document.querySelector("#btnConsultar").addEventListener("click", buscarProducto);
document.querySelector("#btnDeshabilitar").addEventListener("click", quitarProducto);
document.querySelector("#btnTerminar").addEventListener("click", terminarVenta);
document.querySelector("#btnLimpiar").addEventListener("click", limpiarCampos);
document.querySelector("#cerrarSesion").addEventListener("click", async e => {
	e.preventDefault();

	await signOut(auth);
});
