"use strict";
// Import the functions you need from the SDKs you need
import { onValue, ref } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { db } from "./config.js";

// Initialize Firebase
const productos = document.querySelector("#productos");

async function cargarProductos() {
	try {
		event.preventDefault();

		productos.innerHTML = "";
		const dbref = ref(db, "productos");

		await onValue(dbref, snapshot => {
			snapshot.forEach(childSnapshot => {
				const childData = childSnapshot.val();

				if (childData.status === "1") {
					productos.innerHTML += `<div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-3 p-5">
								<h5 class="my-0">${childData.nombre}</h5>
								<img src="${childData.url}" class="w-100" alt="Imagen de ${childData.nombre}">
								<p class="my-2">${childData.descripcion}</p>
								<b class="d-block text-end my-2">${childData.precio}</b>
							<div class="text-end">
								<button class="btn btn-success" onclick="alert('Pedido realizado')">Comprar</button>
							</div>
					</div>`;
				}
			});
		});
	} catch (error) {
		console.error(error);
	}
}

window.addEventListener("DOMContentLoaded", cargarProductos);
